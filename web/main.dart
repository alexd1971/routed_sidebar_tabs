import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:routed_sidebar_tabs/app_component.template.dart' as ng;

import 'main.template.dart' as self;

@GenerateInjector([
  routerProvidersHash,
])
// ignore: undefined_prefixed_name
final InjectorFactory injector = self.injector$Injector;
void main() {
  runApp(ng.AppComponentNgFactory, createInjector: injector);
}
