# Компоненты SideBar и TabBar

Пример реализации компонентов  анимированного `SideBar` и `TabBar` которые управляются через маршрутизацию и следовательно позволяют переходить на них по прямой ссылке.

Основной упор сделан на разработку логики компонентов. Шаблоны и стили требуют доработки.
