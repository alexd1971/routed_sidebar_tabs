import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';

import 'routes.dart';
import 'src/side_bar/side_bar.dart';

@Component(
  selector: 'my-app',
  styleUrls: ['app_component.css'],
  templateUrl: 'app_component.html',
  exports: [
    Routes,
  ],
  directives: [
    SideBar,
    routerDirectives,
  ],
)
class AppComponent {}
