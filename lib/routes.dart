import 'package:angular_router/angular_router.dart';
import 'src/some_module/some_module.template.dart' as module_template;

import 'route_paths.dart';

class Routes {
  static final module = RouteDefinition(
    routePath: RoutePaths.module,
    component: module_template.SomeModuleNgFactory,
  );

  static List<RouteDefinition> get all => [
        module,
      ];
}
