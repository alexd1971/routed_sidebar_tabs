import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';

import '../tab/tab.dart';

@Component(
  selector: 'tab-bar',
  templateUrl: 'tab_bar.html',
  styleUrls: ['tab_bar.css'],
  directives: [
    coreDirectives,
    routerDirectives,
  ],
)
class TabBar {
  @ContentChildren(Tab)
  List<Tab> tabs;

  @Input()
  List<RouteDefinition> routes;
}
