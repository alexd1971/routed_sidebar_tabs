import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';

import '../../route_paths.dart' as app;
import 'route_paths.dart';
import 'routes.dart';

/// Корневой компонент некоторого модуля
///
/// Компонент реализует интерфейс [CanReuse], так как компонент не должен уничтожаться
/// при переходе по внутренним маршрутам модуля
@Component(
  selector: 'some-module',
  templateUrl: 'some_module.html',
  directives: [
    routerDirectives,
  ],
  exports: [
    Routes,
    RoutePaths,
  ],
)
class SomeModule implements CanReuse {
  @override
  Future<bool> canReuse(current, next) async {
    if (next.routePath.toUrl().startsWith(app.RoutePaths.module.toUrl())) {
      return true;
    }
    return false;
  }
}
