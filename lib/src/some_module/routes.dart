import 'package:angular_router/angular_router.dart';
import 'route_paths.dart';
import '../viewer/viewer.template.dart' as viewer_template;

class Routes {
  static final view = RouteDefinition(
      routePath: RoutePaths.view, component: viewer_template.ViewerNgFactory);

  static final all = [
    view,
  ];
}
