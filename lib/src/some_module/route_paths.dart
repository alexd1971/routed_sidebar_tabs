import 'package:angular_router/angular_router.dart';
import '../../route_paths.dart' as app;

class RoutePaths {
  static final view = RoutePath(path: 'view', parent: app.RoutePaths.module);
}
