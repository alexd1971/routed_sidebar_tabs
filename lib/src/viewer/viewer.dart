import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';

import '../tab/tab.dart';
import '../tab_bar/tab_bar.dart';
import '../side_bar/side_bar.dart';
import 'routes.dart' as viewer;
import '../../routes.dart' as app;
import 'route_paths.dart';
import '../some_module/route_paths.dart' as module;

/// Компонент просмотра
///
/// Использует в шаблоне компоненты [SideBar], [TabBar] и [Tab].
/// При маршрутизации на этот компонент появляется SideBar. При закрытии
/// [SideBar] возвращается на корневой компонент модуля
///
/// Реализует интерфейсы:
/// - [OnActivate] для открывания [SideBar]
/// - [CanDeactivate] для того, чтобы маршрутизатор дождался закрытия [SideBar]
/// перед уходом с компонента [Viewer]
/// - [CanReuse] для того, чтобы компонент [Viewer] не пересоздавался при
/// переходе по табам
@Component(
  selector: 'viewer',
  templateUrl: 'viewer.html',
  styleUrls: ['viewer.css'],
  exports: [
    viewer.Routes,
    RoutePaths,
  ],
  directives: [
    SideBar,
    TabBar,
    Tab,
  ],
)
class Viewer implements OnActivate, CanDeactivate, CanReuse {
  final Router _router;

  Viewer(this._router);

  @ViewChild(SideBar)
  SideBar sidebar;

  /// Переходит на корневой компонент модуля
  ///
  /// При этом автоматически вызывается метод [canDeactivate], который
  /// закрывает [SideBar]
  void close() {
    _router.navigate(app.Routes.module.toUrl());
  }

  @override
  void onActivate(previous, current) {
    // Открытие [SideBar] должно происходить только если он еще не открыт
    // Это соответсвует тому, что предыдущий маршрут не содержит в себе
    // путь /module/view
    if (previous?.routePath
            ?.toUrl()
            ?.startsWith(module.RoutePaths.view.toUrl()) ==
        true) {
      return;
    }
    sidebar.open();
  }

  @override
  Future<bool> canDeactivate(current, next) async {
    //Если уходим из [Viewer], то закрываем [SideBar]
    if (!next.routePath.toUrl().startsWith(module.RoutePaths.view.toUrl())) {
      await sidebar.close();
    }
    return true;
  }

  @override
  Future<bool> canReuse(current, next) async {
    if (next.routePath.toUrl().startsWith(module.RoutePaths.view.toUrl())) {
      return true;
    }
    return false;
  }
}
