import 'package:angular_router/angular_router.dart';

import '../some_module/route_paths.dart' as some_module;

class RoutePaths {
  static final tab1 = RoutePath(
    path: 'tab1',
    parent: some_module.RoutePaths.view,
    useAsDefault: true,
  );

  static final tab2 = RoutePath(
    path: 'tab2',
    parent: some_module.RoutePaths.view,
  );

  static final tab3 = RoutePath(
    path: 'tab3',
    parent: some_module.RoutePaths.view,
  );
}
