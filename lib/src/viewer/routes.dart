import 'package:angular_router/angular_router.dart';

import 'route_paths.dart';
import 'tab_content/tab1.template.dart' as tab1_template;
import 'tab_content/tab2.template.dart' as tab2_template;
import 'tab_content/tab3.template.dart' as tab3_template;

class Routes {
  static final tab1 = RouteDefinition(
    routePath: RoutePaths.tab1,
    component: tab1_template.Tab1NgFactory,
  );

  static final tab2 = RouteDefinition(
    routePath: RoutePaths.tab2,
    component: tab2_template.Tab2NgFactory,
  );

  static final tab3 = RouteDefinition(
    routePath: RoutePaths.tab3,
    component: tab3_template.Tab3NgFactory,
  );

  static final all = [
    tab1,
    tab2,
    tab3,
  ];
}
