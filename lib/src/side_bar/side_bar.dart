import 'dart:html';

import 'package:angular/angular.dart';

/// Анимированный side bar
///
/// Side bar выезжает справа
@Component(
  selector: 'side-bar',
  templateUrl: 'side_bar.html',
  styleUrls: ['side_bar.css'],
)
class SideBar {
  /// Html-элемент компонента
  ///
  /// Используется для выполнения анимации
  @ViewChild('sidebar')
  Element sidebar;

  /// Открывает side bar
  ///
  /// Возвращает [Future], который завершается после завершения анимации
  Future<void> open() async {
    final animation = sidebar.animate([
      {'transform': 'translate(0)'},
      {'transform': 'translate(-100%)'}
    ], 300);
    animation.onFinish.listen((_) {
      sidebar.classes.remove('closed');
      sidebar.classes.add('open');
    });
    await animation.finished;
  }

  /// Закрывает side bar
  ///
  /// Возвращает [Future], который завершается после завершения анимации
  Future<void> close() async {
    final animation = sidebar.animate([
      {'transform': 'translate(0)'},
      {'transform': 'translate(100%)'}
    ], 300);
    animation.onFinish.listen((_) {
      sidebar.classes.remove('open');
      sidebar.classes.add('closed');
    });
    await animation.finished;
  }
}
