import 'dart:html';

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';

/// Элемент [TabBar]
@Component(
  selector: 'tab',
  templateUrl: 'tab.html',
  styleUrls: ['tab.css'],
  directives: [
    routerDirectives,
  ],
)
class Tab {
  final Router _router;

  Tab(this._router);

  /// HTML-элемент компонента
  ///
  /// Используется для динамического управления [title]
  @ViewChild('tab')
  Element tabElement;

  /// Маршрут, соответствующий табу
  @Input()
  RoutePath route;

  /// Признак активности таба
  bool get isActive => _router.current?.routePath?.toUrl() == route?.toUrl();

  /// Заголовок
  String get title => tabElement?.innerText;
  set title(String value) {
    tabElement?.innerText = value;
  }

  bool _disabled = false;

  /// Признак недоступности таба
  ///
  /// Таб недоступен при явной установке данного свойства в `true`
  /// либо, если не указан маршрут для таба
  bool get disabled => _disabled || route == null;
  set disabled(bool value) {
    _disabled = value;
  }
}
